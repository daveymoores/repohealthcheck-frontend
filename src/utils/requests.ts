export interface Header {
  authorization: string | null;
  contentType: string;
}

export interface Body {
  username: string;
  password: string;
}

export interface Options {
  method: string;
  headers: {
    'content-type': string;
    authorization: string | null;
  };
  mode: string;
  body: any;
  url: string;
}

export function createRequest(request: Options): Promise<any> {
  const options: any = {
    method: request.method,
    withCredentials: true,
    credentials: 'include',
    headers: {
      'Content-Type': request.headers['content-type'],
      Authorization: request.headers.authorization
    },
    body: JSON.stringify(request.body)
  };

  console.log(options);
  return fetch(request.url, options);
}

export async function sendRequest(request: Options): Promise<any> {
  if (!request.url) return console.error('No url sent to fetch');

  try {
    return await createRequest(request);
  } catch (err) {
    console.error(err);
  }
}
