import * as actions from '../actions/constants';
import { SigninAction } from './types';

const userInitialState = {
  url: '',
  body: {},
  status: '',
  message: '',
  signedin: false
};

export function signinReducer(state = userInitialState, action: SigninAction) {
  switch (action.type) {
    case actions.SIGNIN_USER_REQUEST:
      return Object.assign({}, state, {
        url: action.payload.url,
        body: action.payload.body
      });
    case actions.SIGNIN_USER_SUCCESS:
      return Object.assign({}, state, {
        status: action.payload.status,
        signedin: action.payload.signedin
      });
    case actions.SIGNIN_USER_FAILED:
      return Object.assign({}, state, {
        message: action.payload.status
      });
    default:
      return state;
  }
}
