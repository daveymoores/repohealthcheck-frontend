import * as actions from '../actions/constants';
import { RepositoryAction } from './types';

const repositoryInitialState = {
  message: '',
  repositories: []
};

export function repositoryReducer(
  state = repositoryInitialState,
  action: RepositoryAction
) {
  switch (action.type) {
    case actions.REPOSITORY_REQUEST:
      return Object.assign({}, state, {
        url: action.payload.url,
        body: action.payload.body
      });
    case actions.REPOSITORY_SUCCESS:
      return Object.assign({}, state, {
        repositories: action.payload.repositories
      });
    case actions.REPOSITORY_FAILED:
      return Object.assign({}, state, {
        message: action.payload.status
      });
    default:
      return state;
  }
}
