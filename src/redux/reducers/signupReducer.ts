import * as actions from '../actions/constants';
import { SignupAction } from './types';

const userInitialState = {
  url: '',
  body: {},
  status: '',
  message: '',
  signedup: false
};

export function signupReducer(state = userInitialState, action: SignupAction) {
  switch (action.type) {
    case actions.SIGNUP_USER_REQUEST:
      return Object.assign({}, state, {
        url: action.payload.url,
        body: action.payload.body
      });
    case actions.SIGNUP_USER_SUCCESS:
      return Object.assign({}, state, {
        status: action.payload.status,
        signedup: action.payload.signedup
      });
    case actions.SIGNUP_USER_FAILED:
      return Object.assign({}, state, {
        message: action.payload.status
      });
    default:
      return state;
  }
}
