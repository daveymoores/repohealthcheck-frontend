import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { signinReducer } from './signinReducer';
import { signupReducer } from './signupReducer';
import { authReducer } from './authReducer';
import { repositoryReducer } from './repositoryReducer';

const createRootReducer = (history: any) =>
  combineReducers({
    router: connectRouter(history),
    signup: signupReducer,
    signin: signinReducer,
    auth: authReducer,
    repos: repositoryReducer
  });

export default createRootReducer;
