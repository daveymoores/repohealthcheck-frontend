import * as actions from '../actions/constants';
import { AuthAction } from './types';

const authInitialState = {
  token: '',
  userId: '',
  loggedIn: false
};

export function authReducer(state = authInitialState, action: AuthAction) {
  if (action.type === actions.SET_AUTH) {
    return Object.assign({}, state, {
      token: action.payload.token,
      userId: action.payload.userId,
      loggedIn: action.payload.loggedIn
    });
  }

  return state;
}
