export interface SignupPayload {
  url: string;
  body: {
    password: string;
    username: string;
  };
}

export interface SigninPayload {
  url: string;
  body: {
    password: string;
    username: string;
  };
}

export interface RepoRequest {
  provider: string;
  username: string;
}

export interface RepositoryPayload {
  url: string;
  repositories?: any[];
  body: RepoRequest[];
  token: string;
}

export interface Message {
  message: string;
}

export interface SignupAction {
  payload: SignupPayload &
    Message & { signedup: boolean } & {
      status: string;
    };
  type: string;
}

export interface SigninAction {
  payload: SigninPayload &
    Message & { signedin: boolean } & {
      status: string;
    };
  type: string;
}

export interface AuthAction {
  payload: Auth;
  type: string;
}

export interface Auth {
  loggedIn: string;
  userId: string;
  token: string;
}

export interface RepositoryAction {
  payload: RepositoryPayload & {
    status: string;
  };
  type: string;
}
