import * as actions from '../constants';
import { BaseAction } from '../types';
import { RepositoryAction } from '../../reducers/types';

export const repositoryRequest = (state: RepositoryAction): BaseAction => ({
  type: actions.REPOSITORY_REQUEST,
  payload: state
});

export const repositorySuccess = (state: any) => ({
  type: actions.REPOSITORY_SUCCESS,
  payload: state
});

export const repositoryFailure = (state: any) => ({
  type: actions.REPOSITORY_FAILED,
  payload: state
});
