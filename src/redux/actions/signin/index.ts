import * as actions from '../constants';
import { BaseAction } from '../types';
import { SigninPayload } from '../../reducers/types';

export const signinUserRequest = (state: SigninPayload): BaseAction => ({
  type: actions.SIGNIN_USER_REQUEST,
  payload: state
});

export const signinUserSuccess = (state: any) => ({
  type: actions.SIGNIN_USER_SUCCESS,
  payload: state
});

export const signinUserFailure = (state: any) => ({
  type: actions.SIGNIN_USER_FAILED,
  payload: state
});
