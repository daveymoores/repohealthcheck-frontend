import * as actions from '../constants';

export const setAuthState = (state: any) => ({
  type: actions.SET_AUTH,
  payload: state
});
