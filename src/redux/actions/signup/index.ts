import * as actions from '../constants';
import { BaseAction } from '../types';
import { SignupPayload } from '../../reducers/types';

export const signupUserRequest = (state: SignupPayload): BaseAction => ({
  type: actions.SIGNUP_USER_REQUEST,
  payload: state
});

export const signupUserSuccess = (state: any) => ({
  type: actions.SIGNUP_USER_SUCCESS,
  payload: state
});

export const signupUserFailure = (state: any) => ({
  type: actions.SIGNUP_USER_FAILED,
  payload: state
});
