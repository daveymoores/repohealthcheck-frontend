import { SagaIterator } from 'redux-saga';
import { call, put, takeLatest } from 'redux-saga/effects';
import * as actions from '../actions/constants';

import { sendRequest } from '../../utils/requests';

export interface ActionFetchUser {
  payload: any;
}

function* postUser(action: ActionFetchUser): SagaIterator {
  try {
    // @ts-ignore
    const response = yield call(sendRequest, {
      url: action.payload.url,
      method: 'POST',
      body: action.payload.body,
      headers: { 'content-type': 'application/json', authorization: null }
    });

    if (response && !response.ok) {
      return yield put({
        type: actions.SIGNUP_USER_FAILED,
        payload: {
          status: response.statusText,
          signedin: false
        }
      });
    }

    const data = yield response.json();

    if (data) {
      yield put({
        type: actions.SIGNUP_USER_SUCCESS,
        payload: {
          status: data.statusText,
          signedup: true
        }
      });
      yield put({
        type: actions.SET_AUTH,
        payload: {
          token: data.token,
          userId: data.userId,
          loggedIn: true
        }
      });
    }
  } catch (err) {
    yield put({
      type: actions.SIGNUP_USER_FAILED,
      payload: err.message
    });
  }
}

function* signupSaga() {
  // @ts-ignore
  yield takeLatest(actions.SIGNUP_USER_REQUEST, postUser);
}

export default signupSaga;
