import { SagaIterator } from 'redux-saga';
import { call, put, takeLatest } from 'redux-saga/effects';
import * as actions from '../actions/constants';

import { sendRequest } from '../../utils/requests';

export interface ActionFetchRepositories {
  payload: any;
}

function* fetchRepositories(action: ActionFetchRepositories): SagaIterator {
  try {
    // @ts-ignore
    const response = yield call(sendRequest, {
      url: action.payload.url,
      method: 'POST',
      body: action.payload.body,
      headers: {
        'content-type': 'application/json',
        authorization: `Bearer ${action.payload.token}`
      }
    });

    if (response && !response.ok) {
      return yield put({
        type: actions.REPOSITORY_FAILED,
        payload: {
          message: response.statusText
        }
      });
    }

    const data = yield response.json();

    if (data) {
      return yield put({
        type: actions.REPOSITORY_SUCCESS,
        payload: {
          repositories: data.repositories
        }
      });
    }
  } catch (err) {
    yield put({
      type: actions.REPOSITORY_FAILED,
      payload: err.message
    });
  }
}

function* repositorySaga() {
  // @ts-ignore
  yield takeLatest(actions.REPOSITORY_REQUEST, fetchRepositories);
}

export default repositorySaga;
