import { all } from 'redux-saga/effects';
import signupWatcher from './signup';
import repositoryWatcher from './repository';

export default function* Sagas() {
  yield all([signupWatcher(), repositoryWatcher()]);
}
