import React from 'react';
import { Provider } from 'react-redux';
import 'jest-dom/extend-expect';
import { render } from '@testing-library/react';
import configureStore from './redux/store';
import { push } from 'connected-react-router';
import { shallow } from 'enzyme';
import App from './App';

const store = configureStore({});

describe('<App />', () => {
  it('renders without crashing', () => {
    shallow(<App />);
  });

  test('goes to to homepage', () => {
    const { getByTestId } = render(
      <Provider store={store}>
        <App />
      </Provider>
    );
    expect(getByTestId('home')).toHaveTextContent('Home');
  });

  test('goes to to homepage', () => {
    const { getByLabelText } = render(
      <Provider store={store}>
        <App />
      </Provider>
    );

    store.dispatch(push('/dashboard'));
    expect(getByLabelText('Password')).toBeInTheDocument();
  });
});
