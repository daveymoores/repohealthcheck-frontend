import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import UserSignup from './components/userSignup';
import UserSignin from './components/userSignup';
import Home from './components/home';
import Dashboard from './components/dashboard';
import NotFound from './components/notfound';
import { AuthRoute } from './components/authRequiredRoute';
import { history } from './redux/store';

function App({ store }) {
  return (
    <ConnectedRouter history={history}>
      <>
        <Switch>
          <Route exact path='/' render={props => <Home {...props} />} />
          <Route path='/login' render={props => <UserSignin {...props} />} />
          <Route path='/register' render={props => <UserSignup {...props} />} />
          <AuthRoute
            path='/dashboard'
            auth={store}
            render={props => <Dashboard {...props} />}
          />
          <Route path='*' render={props => <NotFound {...props} />} />
        </Switch>
      </>
    </ConnectedRouter>
  );
}

export default App;
