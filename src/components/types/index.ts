import { ChangeEvent } from 'react';

export type HTMLElementChangeEvent<T extends HTMLElement> = ChangeEvent & {
  target: T;
  currentTarget: T;
};
