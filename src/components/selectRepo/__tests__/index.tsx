import React from 'react';
import { shallow } from 'enzyme';
import SelectRepo from '..';

describe('<SelectRepo />', () => {
  const mockProps = {
    repositoryName: 'test name',
    domName: '__testname',
    handleRadioChange: jest.fn(),
    checked: true
  };

  it('renders and mounts', () => {
    const wrapper = shallow(<SelectRepo {...mockProps} />);
    expect(wrapper.exists()).toBeTruthy();
  });

  it('renders two radio buttons', () => {
    const wrapper = shallow(<SelectRepo {...mockProps} />);
    expect(wrapper.find("input[type='radio']").length).toEqual(2);
  });

  it('receives repository name prop and displays it', () => {
    const wrapper = shallow(<SelectRepo {...mockProps} />);
    expect(wrapper.find('.repo_name').text()).toBe('test name');
  });

  it("creates ID's and htmlFor values based on repositoryName", () => {
    const wrapper = shallow(<SelectRepo {...mockProps} />);

    const trimmedValue = `__${mockProps.repositoryName
      .replace(' ', '')
      .toLowerCase()}`;
    expect(
      wrapper
        .find("input[type='radio']")
        .first()
        .props().id
    ).toBe(`keep${trimmedValue}`);
  });
});
