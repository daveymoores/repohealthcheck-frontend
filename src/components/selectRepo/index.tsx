import React from 'react';
import { HTMLElementChangeEvent } from '../types';

export interface Props {
  repositoryName: string;
  handleRadioChange: (event: HTMLElementChangeEvent<HTMLInputElement>) => void;
  domName: string;
  checked: boolean;
}

const SelectRepo = ({
  repositoryName,
  domName,
  checked,
  handleRadioChange
}: Props) => {
  return (
    <div>
      <div className='repo_name'>{repositoryName}</div>
      <label htmlFor={`keep${domName}`}>
        keep
        <input
          name={`${domName}`}
          type='radio'
          id={`keep${domName}`}
          value={`keep${domName}`}
          checked={checked}
          onChange={handleRadioChange}
        />
      </label>
      <label htmlFor={`remove${domName}`}>
        remove
        <input
          name={`${domName}`}
          type='radio'
          id={`remove${domName}`}
          value={`keep${domName}`}
          checked={!checked}
          onChange={handleRadioChange}
        />
      </label>
    </div>
  );
};

export default SelectRepo;
