import React, { SyntheticEvent, useState } from 'react';
import { Redirect } from 'react-router-dom';
import { signupUserRequest } from '../../redux/actions/signup';
import { SignupPayload } from '../../redux/reducers/types';
import { connect } from 'react-redux';
import ErrorMessage from '../ErrorMessage';
import FormInput, { InputTypes } from '../formInput';

interface Props {}

interface InjectedProps extends Props {
  signupUserRequest: any;
  auth: {
    loggedIn: boolean;
  };
}

export const UserSignup: React.FunctionComponent<Props> = props => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = (event: SyntheticEvent): void => {
    event.preventDefault();
    const { signupUserRequest } = props as InjectedProps;
    const requestParams: SignupPayload = {
      body: {
        username,
        password
      },
      url: '/signup'
    };

    // fire dispatch for saga
    signupUserRequest(requestParams);
  };

  const { auth } = props as InjectedProps;
  if (auth.loggedIn) {
    return <Redirect to='/dashboard' />;
  }

  return (
    <section>
      <form data-testid='login' onSubmit={handleSubmit}>
        <FormInput
          handleChange={e => setUsername(e.target.value)}
          type={InputTypes.EMAIL}
          id='username'
          name='username'
          placeholder='username'
          value={username}
          labelText='Your email'
        />
        <FormInput
          handleChange={e => setPassword(e.target.value)}
          type={InputTypes.PASSWORD}
          id='password'
          name='password'
          placeholder='password'
          value={password}
          labelText='Password'
        />
        <button type='submit'>Submit</button>
      </form>
      <ErrorMessage />
    </section>
  );
};

const mapStateToProps = (state: any) => {
  return state;
};

const mapDispatchToProps = {
  signupUserRequest
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserSignup);
