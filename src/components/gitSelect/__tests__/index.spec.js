import React from 'react';
import { mount } from 'enzyme';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import 'jest-dom/extend-expect';
import { repositoryReducer as reducer } from '../../../redux/reducers/repositoryReducer';
import { render, fireEvent } from '@testing-library/react';
import GitSelect, { GitSelectComponent } from '..';

describe('<gitSelect />', () => {
  function renderWithRedux(
    component,
    { initialState, store = createStore(reducer, initialState) } = {}
  ) {
    return {
      ...render(<Provider store={store}>{component}</Provider>)
    };
  }

  it('GitSelect component renders repo names when submitted', () => {
    const mockdataFromStore = {
      auth: {
        userId: '01234567',
        token: 'rf2525g2g5245g2'
      },
      repos: {
        repositories: [
          {
            repositoryName: 'test repo 1'
          }
        ]
      }
    };

    const { getByLabelText, getByText, getByTestId } = renderWithRedux(
      <GitSelect {...mockdataFromStore} />
    );

    const githubUsernameInput = getByLabelText('Github username');
    const bitbucketUsernameInput = getByLabelText('Bitbucket username');
    const githubCheckbox = getByLabelText('Use Github Repos?');
    const bitbucketCheckbox = getByLabelText('Use Bitbucket Repos?');
    const submitButton = getByText('Submit');
    const form = getByTestId('form');
    const repoWrapper = getByTestId('repo-names');

    githubUsernameInput.value = 'daveymoores';
    bitbucketUsernameInput.value = 'daveymoores';
    githubCheckbox.setAttribute('checked', true);
    bitbucketCheckbox.setAttribute('checked', true);

    form.submit = jest.fn();

    fireEvent(
      submitButton,
      new MouseEvent('click', {
        bubbles: true,
        cancelable: true
      })
    );

    expect(repoWrapper).toHaveTextContent('test repo 1');
  });

  it('it takes values in with bitbucket ID on both inputs and only sets one', () => {
    const wrapper = mount(<GitSelectComponent />);

    const mockEvent = {
      target: {
        getAttribute: value => 'bitbucketUsername',
        value: 'mock value'
      }
    };

    wrapper.find('#bitbucketUsername').simulate('change', mockEvent);
    wrapper.find('#githubUsername').simulate('change', mockEvent);

    // remember to not set nodes as variables
    expect(wrapper.find('#bitbucketUsername').props().value).toEqual(
      'mock value'
    );
    expect(wrapper.find('#githubUsername').props().value).toEqual('');
  });

  it('it takes the correct ID on individual inputs and correctly sets value', () => {
    const wrapper = mount(<GitSelectComponent />);

    const BBMockEvent = {
      target: {
        getAttribute: value => 'bitbucketUsername',
        value: 'BitBucketUsername'
      }
    };

    const GHMockEvent = {
      target: {
        getAttribute: value => 'githubUsername',
        value: 'GitHubUsername'
      }
    };

    wrapper.find('#bitbucketUsername').simulate('change', BBMockEvent);
    wrapper.find('#githubUsername').simulate('change', GHMockEvent);

    expect(wrapper.find('#bitbucketUsername').props().value).toEqual(
      'BitBucketUsername'
    );
    expect(wrapper.find('#githubUsername').props().value).toEqual(
      'GitHubUsername'
    );
  });

  it('calls the prop of repositoryRequest and renders repo names', () => {});
});
