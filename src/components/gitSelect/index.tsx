import React, { useState, useEffect, SyntheticEvent } from 'react';
import { connect } from 'react-redux';
import { repositoryRequest } from '../../redux/actions/repository';
import { HTMLElementChangeEvent } from '../types';
import { RepositoryPayload, Auth } from '../../redux/reducers/types';
import GitSelectForm from '../gitSelectForm';
import SelectRepo from '../selectRepo';
import _ from 'lodash';

interface Props {}

interface InjectedProps extends Props {
  repositoryRequest: any;
  auth: Auth;
  repos: {
    repositories: any[];
  };
}

export interface CheckedValue {
  domName: string;
  repositoryName: string;
  checked: boolean;
}

export const GitSelectComponent = (props: Props) => {
  const { repos, repositoryRequest, auth } = props as InjectedProps;

  const [providers, setProviders] = useState<string[]>([]);
  const [bitbucketUsername, setBitbucketUsername] = useState<string>('');
  const [githubUsername, setGithubUsername] = useState<string>('');
  const [checkedValues, setCheckedValues] = useState<any>([]);

  const handleSubmit = (event: SyntheticEvent<HTMLFormElement>): void => {
    event.preventDefault();

    const requestParams: RepositoryPayload = {
      body: providers.map(provider => {
        return {
          provider,
          username:
            provider === 'bitbucket' ? bitbucketUsername : githubUsername
        };
      }),
      url: `http://localhost:8000/api/repo/${auth.userId}`,
      token: auth.token
    };

    repositoryRequest(requestParams);
  };

  const handleChange = (
    event: HTMLElementChangeEvent<HTMLInputElement>
  ): void => {
    const id: string | null = event.target.getAttribute('id') || null;
    if (!id) return;

    if (event.target.type === 'checkbox') {
      const newProviders = providers.concat(id);
      setProviders(newProviders);
      return;
    }

    if (id === 'bitbucketUsername') {
      return setBitbucketUsername(event.target.value);
    }

    return setGithubUsername(event.target.value);
  };

  const handleRadioChange = (
    event: HTMLElementChangeEvent<HTMLInputElement>
  ) => {
    const checkedIndex: number = _.findIndex(checkedValues, {
      domName: event.target.getAttribute('name')
    });

    // set the checked value to the new boolean value
    checkedValues[checkedIndex].checked = !checkedValues[checkedIndex].checked;
    return setCheckedValues([...checkedValues]);
  };

  useEffect(() => {
    const populateCheckedProps = () => {
      if (repos && repos.repositories) {
        const repoDOMProps: CheckedValue[] = repos.repositories.map(
          (repo: { repositoryName: string }, i) => {
            const domName = `__${repo.repositoryName
              .split(' ')
              .join('')
              .toLowerCase()}`;

            return {
              domName,
              checked: true,
              repositoryName: repo.repositoryName
            };
          }
        );

        setCheckedValues((oldCheckedValues: CheckedValue[]) => [
          ...oldCheckedValues,
          ...repoDOMProps
        ]);
      }
    };

    populateCheckedProps();
  }, [repos]);

  return (
    <>
      <GitSelectForm
        handleChange={handleChange}
        handleSubmit={handleSubmit}
        githubValue={githubUsername}
        bitbucketValue={bitbucketUsername}
      />

      <form data-testid='repo-names'>
        {checkedValues.length > 0 &&
          checkedValues.map((repo: CheckedValue, i: string) => {
            const { domName, checked, repositoryName } = repo;
            return (
              <SelectRepo
                key={i}
                domName={domName}
                checked={checked}
                handleRadioChange={handleRadioChange}
                repositoryName={repositoryName}
              />
            );
          })}
      </form>
    </>
  );
};

const mapStateToProps = (state: any) => {
  return state;
};

const mapDispatchToProps = {
  repositoryRequest
};

const GitSelect = connect(
  mapStateToProps,
  mapDispatchToProps
)(GitSelectComponent);

export default GitSelect;
