import React from 'react';
import { mount } from 'enzyme';
import { AuthRequiredRoute } from '../';
import { Router } from 'react-router-dom';
import { history } from '../../../redux/store';

describe('<AuthRequiredRoute />', () => {
  const element = () => <div />;

  const mockProps = {
    render: element,
    auth: {
      loggedIn: true
    }
  };

  it('exist', () => {
    const wrapper = mount(
      <Router history={history}>
        <AuthRequiredRoute {...mockProps} />
      </Router>
    );

    expect(wrapper.exists()).toBeTruthy();
  });
});
