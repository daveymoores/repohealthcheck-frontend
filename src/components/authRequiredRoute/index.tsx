import React, { ReactType } from 'react';
import { connect } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';

interface Props {
  render: ReactType;
  auth: {
    loggedIn: boolean;
  };
}

export const AuthRequiredRoute = ({
  render: Component,
  auth,
  ...rest
}: Props) => {
  return (
    <Route
      {...rest}
      render={props =>
        auth.loggedIn ? <Component {...props} /> : <Redirect to='/login' />
      }
    />
  );
};

const mapStateToProps = (state: any) => {
  return state;
};

export const AuthRoute = connect(mapStateToProps)(AuthRequiredRoute);
