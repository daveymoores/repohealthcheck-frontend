import React, { useState, Component, SyntheticEvent } from 'react';
import { Redirect } from 'react-router-dom';
import { signinUserRequest } from '../../redux/actions/signin';
import { SigninPayload } from '../../redux/reducers/types';
import { connect } from 'react-redux';
import ErrorMessage from '../ErrorMessage';
import FormInput, { InputTypes } from '../formInput';

interface AuthProps {
  signinUserRequest(payload: SigninPayload): void;
  auth: {
    loggedIn: boolean;
  };
}

export function UserSignin({ auth, signinUserRequest }: AuthProps) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = (event: SyntheticEvent): void => {
    event.preventDefault();
    const requestParams: SigninPayload = {
      body: {
        username,
        password
      },
      url: '/signin'
    };

    // fire dispatch for saga
    signinUserRequest(requestParams);
  };

  if (auth.loggedIn) {
    return <Redirect to='/dashboard' />;
  }

  return (
    <section>
      <form data-testid='login' onSubmit={handleSubmit}>
        <FormInput
          handleChange={e => setUsername(e.target.value)}
          type={InputTypes.EMAIL}
          id='username'
          name='username'
          placeholder='username'
          value={username}
          labelText='Your email'
        />
        <FormInput
          handleChange={e => setPassword(e.target.value)}
          type={InputTypes.PASSWORD}
          id='password'
          name='password'
          placeholder='password'
          value={password}
          labelText='Password'
        />
        <button type='submit'>Submit</button>
      </form>
      <ErrorMessage />
    </section>
  );
}

const mapStateToProps = (state: any) => {
  return state;
};

const mapDispatchToProps = {
  signinUserRequest
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserSignin);
