import { mount } from 'enzyme';
import React from 'react';
import { UserSignin } from '../';
jest.mock('../../ErrorMessage');

describe('<UserSignin />', () => {
  const mockInjectedProps = {
    auth: {
      loggedIn: false
    },
    signinUserRequest: jest.fn()
  };

  it('renders dom nodes', () => {
    const wrapper = mount(<UserSignin {...mockInjectedProps} />);
    expect(wrapper.find('#password').exists()).toBeTruthy();
    expect(wrapper.find('#username').exists()).toBeTruthy();
  });

  it('triggers onChange handler when the username input receives text', () => {
    const wrapper = mount(<UserSignin {...mockInjectedProps} />);

    wrapper.find('input[type="email"]').simulate('change', {
      target: {
        value: 'testUsername'
      }
    });

    expect(wrapper.find('input[type="email"]').prop('value')).toEqual(
      'testUsername'
    );
  });

  it('triggers onChange handler when the password input receives text', () => {
    const wrapper = mount(<UserSignin {...mockInjectedProps} />);

    wrapper.find('input[type="password"]').simulate('change', {
      target: {
        value: 'testPassword'
      }
    });

    expect(wrapper.find('input[type="password"]').prop('value')).toEqual(
      'testPassword'
    );
  });

  it('takes a username and password and the form submits', () => {
    const wrapper = mount(<UserSignin {...mockInjectedProps} />);

    const form = wrapper.find('form');
    const usernameInput = wrapper.find('input[type="email"]');
    const passwordInput = wrapper.find('input[type="password"]');

    const mockEvent = {
      preventDefault: () => {}
    };

    const mockState = {
      username: 'username',
      password: 'password'
    };

    usernameInput.prop('onChange')({
      target: {
        value: 'username'
      }
    });
    passwordInput.prop('onChange')({
      target: {
        value: 'password'
      }
    });

    expect(form.exists()).toBeTruthy();
    form.simulate('submit', mockEvent);

    expect(mockInjectedProps.signinUserRequest).toHaveBeenCalledWith({
      body: mockState,
      url: '/signin'
    });
  });
});
