import React from 'react';

const Notfound = () => <div data-testid='not-found'>Not Found</div>;

export default Notfound;
