import React from 'react';

interface Props {
  handleSubmit: (event: React.FormEvent<HTMLFormElement>) => void;
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  githubValue: string;
  bitbucketValue: string;
}

const GitSelectForm: React.FunctionComponent<Props> = ({
  handleSubmit,
  handleChange,
  githubValue,
  bitbucketValue
}) => (
  <form data-testid='form' onSubmit={handleSubmit}>
    <div>
      <label htmlFor='github'>
        Use Github Repos?
        <input type='checkbox' onChange={handleChange} id='github' />
      </label>
      <label htmlFor='githubUsername'>
        Github username
        <input
          type='text'
          value={githubValue}
          onChange={handleChange}
          id='githubUsername'
        />
      </label>
    </div>
    <div>
      <label htmlFor='bitbucket'>
        Use Bitbucket Repos?
        <input type='checkbox' onChange={handleChange} id='bitbucket' />
      </label>
      <label htmlFor='bitbucketUsername'>
        Bitbucket username
        <input
          type='text'
          value={bitbucketValue}
          onChange={handleChange}
          id='bitbucketUsername'
        />
      </label>
    </div>
    <button type='submit'>Submit</button>
  </form>
);

export default GitSelectForm;
