import React from 'react';
import 'jest-dom/extend-expect';
import { render, fireEvent } from '@testing-library/react';
import GitSelectForm from '..';

describe('<gitSelectForm />', () => {
  it('renders GitSelectForm component and submits form', () => {
    const handleSubmit = jest.fn();
    const handleChange = jest.fn();

    const { getByTestId, getByLabelText } = render(
      <GitSelectForm handleChange={handleChange} handleSubmit={handleSubmit} />
    );

    const githubUsernameInput = getByLabelText('Github username');
    const bitbucketUsernameInput = getByLabelText('Bitbucket username');
    const githubCheckbox = getByLabelText('Use Github Repos?');
    const bitbucketCheckbox = getByLabelText('Use Bitbucket Repos?');

    githubUsernameInput.value = 'daveymoores';
    bitbucketUsernameInput.value = 'daveymoores';
    githubCheckbox.setAttribute('checked', true);
    bitbucketCheckbox.setAttribute('checked', true);

    fireEvent.submit(getByTestId('form'));

    expect(handleSubmit).toHaveBeenCalledTimes(1);
  });
});
