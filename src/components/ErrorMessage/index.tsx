import React from 'react';
import { connect } from 'react-redux';

interface Props {}

interface InjectedProps extends Props {
  message: string;
}

export const ErrorMessage: React.FunctionComponent<Props> = props => {
  const { message } = props as InjectedProps;
  if (message) {
    return (
      <div>
        <p>{message}</p>
      </div>
    );
  }

  return null;
};

export const mapStateToProps = (state: any) => {
  const message =
    state.router.location.pathname === '/register'
      ? state.signup.message
      : state.signin.message;
  return {
    message
  };
};

export default connect(mapStateToProps)(ErrorMessage);
