import React from 'react';
import { shallow } from 'enzyme';
import { mapStateToProps } from '..';
import { ErrorMessage } from '..';

describe('<ErrorMessage /> ', () => {
  const mockProps = {
    signup: {
      message: 'test'
    }
  };

  it('exists', () => {
    const wrapper = shallow(<ErrorMessage {...mockProps.signup} />);
    expect(wrapper.exists()).toBeTruthy();
  });

  it('should render text when it recieves props', () => {
    const wrapper = shallow(<ErrorMessage {...mockProps.signup} />);
    expect(wrapper.find('p').text()).toEqual('test');
  });

  it('should return the correct state from mapStateToProps', () => {
    const state = {
      signin: {
        message: 'Signin Error'
      },
      signup: {
        message: 'Signup Error'
      }
    };

    expect(
      mapStateToProps(
        Object.assign({}, state, {
          router: {
            location: {
              pathname: '/register'
            }
          }
        })
      )
    ).toMatchObject({
      message: 'Signup Error'
    });

    expect(
      mapStateToProps(
        Object.assign({}, state, {
          router: {
            location: {
              pathname: '/login'
            }
          }
        })
      )
    ).toMatchObject({
      message: 'Signin Error'
    });
  });
});
