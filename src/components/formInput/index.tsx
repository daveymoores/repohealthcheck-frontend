import React, { useEffect, useState } from 'react';
import * as EmailValidator from 'email-validator';
import ValidatePassword from 'validate-password';

export enum InputTypes {
  TEXT = 'text',
  PASSWORD = 'password',
  EMAIL = 'email'
}

export interface Props {
  handleChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  type: InputTypes;
  name: string;
  placeholder?: string;
  id: string;
  value: string;
  labelText: string;
}

var validator = new ValidatePassword();

const FormInput: React.FunctionComponent<Props> = ({
  type = InputTypes.TEXT,
  name,
  placeholder = '',
  handleChange,
  id,
  value,
  labelText
}) => {
  const [errorMessage, setErrorMessage] = useState('');
  const [showError, setShowError] = useState(false);

  const validateEmail = (value: string): void => {
    if (!EmailValidator.validate(value)) {
      setShowError(true);
      setErrorMessage('That email address is invalid');
      return;
    }
    return setShowError(false);
  };

  const validatePassword = (value: string): void => {
    var passwordData = validator.checkPassword(value);
    if (!passwordData.isValid) {
      setShowError(true);
      setErrorMessage(passwordData.validationMessage);
      return;
    }
    return setShowError(false);
  };

  useEffect(() => {
    switch (type) {
      case InputTypes.EMAIL:
        validateEmail(value);
        break;
      case InputTypes.TEXT:
        setErrorMessage('');
        break;
      case InputTypes.PASSWORD:
        validatePassword(value);
        break;
      default:
        break;
    }
  }, [value, type]);

  return (
    <div>
      <label htmlFor={id}>
        {labelText}
        <input
          type={type}
          onChange={handleChange}
          name={name}
          placeholder={placeholder}
          value={value}
          id={id}
        />
      </label>
      {showError && <span className='error'>{errorMessage}</span>}
    </div>
  );
};

export default FormInput;
