import React from 'react';
import { mount } from 'enzyme';
import FormInput, { InputTypes, Props } from '../';

describe('<FormInput />', () => {
  const mockProps: Props = {
    name: 'test input',
    type: InputTypes.EMAIL,
    handleChange: jest.fn(),
    id: 'form_input_firstname',
    value: 'mock value',
    labelText: 'some label'
  };

  it('exists', () => {
    const wrapper = mount(<FormInput {...mockProps} />);
    expect(wrapper.exists()).toBeTruthy();
  });

  it('takes props and displays them on the input', () => {
    const wrapper = mount(<FormInput {...mockProps} />);
    expect(wrapper.find('input').props()).toEqual({
      name: mockProps.name,
      type: mockProps.type,
      placeholder: '',
      onChange: mockProps.handleChange,
      id: mockProps.id,
      value: mockProps.value
    });
  });

  it('takes an htmlFor on the label element that matches the input element id', () => {
    const wrapper = mount(<FormInput {...mockProps} />);
    expect(wrapper.find('label').props().htmlFor).toEqual(
      wrapper.find('input').props().id
    );
  });

  it('takes an incorrect value for email type and displays an error message', () => {
    const wrapper = mount(<FormInput {...mockProps} />);
    wrapper.find('input').props().value = 'test.test.test.test';
    expect(wrapper.find('.error').text()).toEqual(
      'That email address is invalid'
    );
  });

  it('takes an incorrect value for password and returns the error message', () => {
    const passwordProps = Object.assign({}, mockProps, {
      value: 'aaaaa',
      type: InputTypes.PASSWORD
    });

    const wrapper = mount(<FormInput {...passwordProps} />);
    expect(wrapper.find('.error').text()).toEqual(
      'The password must contain at least one uppercase letter'
    );
  });
});
